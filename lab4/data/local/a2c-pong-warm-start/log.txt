[2019-08-29 13:30:53.897134 UTC] Starting env pool
[2019-08-29 13:30:55.533876 UTC] Starting epoch 841
------------------------------------
| Epoch                | 841       |
| TotalNEpisodes       | 0         |
| TotalNSamples        | 0         |
| Entropy              | 1.4534    |
| Perplexity           | 4.2777    |
| AveragePolicyProb[0] | 0.14644   |
| AveragePolicyProb[1] | 0.1567    |
| AveragePolicyProb[2] | 0.29741   |
| AveragePolicyProb[3] | 0.10278   |
| AveragePolicyProb[4] | 0.29515   |
| AveragePolicyProb[5] | 0.0015186 |
| |VfPred|             | 2.865     |
| |VfTarget|           | 2.8441    |
| VfLoss               | 0.041828  |
------------------------------------
[2019-08-29 13:31:21.417294 UTC] Saving snapshot
[2019-08-29 13:31:21.418367 UTC] Starting epoch 842
-----------------------------------
| Epoch                | 842      |
| AverageReturn        | -20.875  |
| MinReturn            | -21      |
| MaxReturn            | -20      |
| StdReturn            | 0.33072  |
| AverageEpisodeLength | 3110.4   |
| MinEpisodeLength     | 3056     |
| MaxEpisodeLength     | 3615     |
| StdEpisodeLength     | 150.39   |
| TotalNEpisodes       | 16       |
| TotalNSamples        | 49766    |
| Entropy              | 1.3418   |
| Perplexity           | 3.8258   |
| AveragePolicyProb[0] | 0.20445  |
| AveragePolicyProb[1] | 0.12763  |
| AveragePolicyProb[2] | 0.30902  |
| AveragePolicyProb[3] | 0.21214  |
| AveragePolicyProb[4] | 0.13339  |
| AveragePolicyProb[5] | 0.013383 |
| |VfPred|             | 2.1692   |
| |VfTarget|           | 2.134    |
| VfLoss               | 0.12987  |
-----------------------------------
[2019-08-29 13:31:46.840490 UTC] Saving snapshot
[2019-08-29 13:31:46.841703 UTC] Starting epoch 843
-------------------------------------
| Epoch                | 843        |
| AverageReturn        | -20.633    |
| MinReturn            | -21        |
| MaxReturn            | -20        |
| StdReturn            | 0.48189    |
| AverageEpisodeLength | 3390.6     |
| MinEpisodeLength     | 3056       |
| MaxEpisodeLength     | 4332       |
| StdEpisodeLength     | 381.48     |
| TotalNEpisodes       | 30         |
| TotalNSamples        | 1.0172e+05 |
| Entropy              | 1.4432     |
| Perplexity           | 4.2341     |
| AveragePolicyProb[0] | 0.1696     |
| AveragePolicyProb[1] | 0.13236    |
| AveragePolicyProb[2] | 0.17308    |
| AveragePolicyProb[3] | 0.29961    |
| AveragePolicyProb[4] | 0.19026    |
| AveragePolicyProb[5] | 0.03508    |
| |VfPred|             | 1.7915     |
| |VfTarget|           | 1.7739     |
| VfLoss               | 0.086356   |
-------------------------------------
[2019-08-29 13:32:14.042831 UTC] Saving snapshot
[2019-08-29 13:32:14.044247 UTC] Starting epoch 844
-------------------------------------
| Epoch                | 844        |
| AverageReturn        | -20.406    |
| MinReturn            | -21        |
| MaxReturn            | -17        |
| StdReturn            | 0.9956     |
| AverageEpisodeLength | 3491.2     |
| MinEpisodeLength     | 3056       |
| MaxEpisodeLength     | 5312       |
| StdEpisodeLength     | 542.48     |
| TotalNEpisodes       | 32         |
| TotalNSamples        | 1.1172e+05 |
| Entropy              | 1.5072     |
| Perplexity           | 4.5141     |
| AveragePolicyProb[0] | 0.17772    |
| AveragePolicyProb[1] | 0.18305    |
| AveragePolicyProb[2] | 0.10481    |
| AveragePolicyProb[3] | 0.26983    |
| AveragePolicyProb[4] | 0.20431    |
| AveragePolicyProb[5] | 0.060276   |
| |VfPred|             | 0.98577    |
| |VfTarget|           | 1.0055     |
| VfLoss               | 0.1167     |
-------------------------------------
[2019-08-29 13:32:41.786577 UTC] Saving snapshot
[2019-08-29 13:32:41.788227 UTC] Starting epoch 845
-------------------------------------
| Epoch                | 845        |
| AverageReturn        | -20.406    |
| MinReturn            | -21        |
| MaxReturn            | -17        |
| StdReturn            | 0.9956     |
| AverageEpisodeLength | 3491.2     |
| MinEpisodeLength     | 3056       |
| MaxEpisodeLength     | 5312       |
| StdEpisodeLength     | 542.48     |
| TotalNEpisodes       | 32         |
| TotalNSamples        | 1.1172e+05 |
| Entropy              | 1.5898     |
| Perplexity           | 4.903      |
| AveragePolicyProb[0] | 0.15395    |
| AveragePolicyProb[1] | 0.17801    |
| AveragePolicyProb[2] | 0.1022     |
| AveragePolicyProb[3] | 0.2202     |
| AveragePolicyProb[4] | 0.16342    |
| AveragePolicyProb[5] | 0.18222    |
| |VfPred|             | 0.69592    |
| |VfTarget|           | 0.73662    |
| VfLoss               | 0.065254   |
-------------------------------------
[2019-08-29 13:33:09.887538 UTC] Saving snapshot
[2019-08-29 13:33:09.889023 UTC] Starting epoch 846
